# Thrift connection pool.

Clone từ https://github.com/wmz7year/Thrift-Connection-Pool.git (toàn chữ Tàu thôi 😆)

Cách dùng:

```java
class Example {
  void demo(){
  ThriftConnectionPoolConfig config = new ThriftConnectionPoolConfig();
        config.setConnectTimeout(3000);
        config.setThriftProtocol(ThriftConnectionPoolConfig.TProtocolType.BINARY);
        config.setClientClass(Example.Client.class);
        config.setTransportProvider(TTransportProvider.TCP_PROVIDER);
        config.setTestDecisionStrategy(() -> true);
        config.<ThriftConnection<Example.Client>>setTestConnectionFunction(thriftConnection -> thriftConnection.getClient().ping());
        for (ThriftServerInfo thriftServerInfo : servers) {
            config.addThriftServer(thriftServerInfo.getHost(), thriftServerInfo.getPort());
        }
        config.setMaxConnectionPerServer(2);
        config.setMinConnectionPerServer(1);
        config.setIdleMaxAge(2, TimeUnit.SECONDS);
        config.setMaxConnectionAge(2);
        config.setLazyInit(false);
        ThriftConnectionPool<Example.Client> pool = new ThriftConnectionPool<Example.Client>(config);
        
}
}
```


