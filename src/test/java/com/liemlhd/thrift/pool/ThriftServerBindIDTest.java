/**
 *  				Copyright 2015 Jiang Wei
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.liemlhd.thrift.pool;

import com.liemlhd.thrift.pool.config.ThriftConnectionPoolConfig;
import com.liemlhd.thrift.pool.config.ThriftServerInfo;
import com.liemlhd.thrift.pool.connection.ThriftConnection;
import com.liemlhd.thrift.pool.example.Example;
import com.liemlhd.thrift.pool.example.Other;
import com.liemlhd.thrift.transport.TTransportProvider;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/*
 * thrift服务器添加可选的ID表识别  可以根据ID获取对应的服务器
 */
public class ThriftServerBindIDTest extends BasicAbstractTest {

    private List<ThriftServerInfo> servers;

    /*
	 * @see com.wmz7year.thrift.pool.BasicAbstractTest#beforeTest()
     */
    @Override
    protected void beforeTest() throws Exception {
        this.servers = startServers(2);
    }

    /*
	 * @see com.wmz7year.thrift.pool.BasicAbstractTest#afterTest()
     */
    @Override
    protected void afterTest() throws Exception {
        // ignore
    }


    public void test(){

    }
}
