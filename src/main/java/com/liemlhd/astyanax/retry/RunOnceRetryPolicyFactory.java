package com.liemlhd.astyanax.retry;

import com.liemlhd.astyanax.retry.RetryPolicy.RetryPolicyFactory;

public class RunOnceRetryPolicyFactory implements RetryPolicyFactory {

	@Override
	public RetryPolicy createRetryPolicy() {
		return new RunOnce();
	}
}
